/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SRC.COM.KHANNA.PRINCE.MTECH;

import javax.crypto.spec.SecretKeySpec;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Prince Khanna
 * @VIT ID 16MIN0361
 * @Wipro ID PR335600
 * @EmailID Prince.khanna@wipro.com
 * @Mobile No 8882071901
 */
public class LETS_DECRYPTTest {
    public static Logger log = Logger.getLogger(RUNNING_FILE.class);
    public LETS_DECRYPTTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of decrypt method, of class LETS_DECRYPT.
     */
    @Test
    public void testDecrypt() {
        BasicConfigurator.configure();
        System.out.println("decrypt");
        String cipherText = "fM3RhhznVF8PK5Iy1WU4r4St04GPFi9Xx3AsfpKNzaQMOuafZ/t8G7IV9Vsucwr/";
        String LETS_PUT_KEY = "KHANNAPRINCE4711";
        SecretKeySpec keySpec = new SecretKeySpec(LETS_PUT_KEY.getBytes(),"AES");
        LETS_DECRYPT instance = new LETS_DECRYPT();
        String expResult = "Prince Khanna MTECH Project Junit Test Cases!";
        String result = instance.LETS_HERE_DECRY_OUR_TEXT(cipherText, keySpec);
        if (expResult.equals(result)){
            System.out.println("Here Is our Expected Results" + expResult );
            System.out.println("Here Is our Actual Results" + result );
            log.info("Here we have our First Logging Statement with Expected Results:------>" + expResult);
            log.info("Here we have our Second Logging Statement with Actual Results:------>" + expResult);
        } else if (expResult == null ? result != null : !expResult.equals(result)){
            System.out.println("Sorry Expected Results and Actual Results Not Matached!");
        }
    }
    
}
