package SRC.COM.KHANNA.PRINCE.MTECH;

import javax.crypto.spec.SecretKeySpec;
import org.apache.log4j.BasicConfigurator;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.apache.log4j.Logger;

/**
 *
 * @author Prince Khanna
 * @VIT ID 16MIN0361
 * @Wipro ID PR335600
 * @EmailID Prince.khanna@wipro.com
 * @Mobile No 8882071901
 */
public class LETS_ENCRYPT_AGNET_USER_PASSWORD_AT_DB_LEVELTest {
    public static Logger log = Logger.getLogger(RUNNING_FILE.class); 
    public LETS_ENCRYPT_AGNET_USER_PASSWORD_AT_DB_LEVELTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of encrypt method, of class LETS_ENCRYPT_AGNET_USER_PASSWORD_AT_DB_LEVEL.
     */
    @org.junit.Test
    public void testEncrypt() {
        BasicConfigurator.configure();
        System.out.println("encrypt");
        String PlainText = "Prince Khanna MTECH Project Junit Test Cases!";
        String LETS_PUT_KEY = "KHANNAPRINCE4711";
        SecretKeySpec keySpec = new SecretKeySpec(LETS_PUT_KEY.getBytes(),"AES");
        LETS_ENCRYPT_AGNET_USER_PASSWORD_AT_DB_LEVEL instance = new LETS_ENCRYPT_AGNET_USER_PASSWORD_AT_DB_LEVEL();
        String expResult = "fM3RhhznVF8PK5Iy1WU4r4St04GPFi9Xx3AsfpKNzaQMOuafZ/t8G7IV9Vsucwr/";
        String result = instance.LETS_GENERATE_ENCRYPT_VALUES(PlainText, keySpec);
        if (expResult.equals(result)){
            System.out.println("Here Is our Expected Results" + expResult );
            System.out.println("Here Is our Actual Results" + result );
            log.info("Here we have our First Logging Statement with Expected Results:------>" + expResult);
            log.info("Here we have our Second Logging Statement with Actual Results:------>" + expResult);
        } else if (expResult == null ? result != null : true){
            System.out.println("Sorry Expected Results and Actual Results Not Matached!");
        }
    }
    
}
