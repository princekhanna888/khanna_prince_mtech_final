<%-- 
    Document   : START_UP_FILE_TRIGGER
    Created on : Jan 13, 2020, 7:59:56 AM
    Author     : M15309
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link href="DESIGN_CODE_CSS/FIRSTSTYLE.css" rel="stylesheet" type="text/css"/>
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>DATA LEAKAGE DETECTION OF FRAUD AGENTS AND SECURITY USING ENCRYPTION ALGORITHM MAC ADDRESS</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

    </head>
    <body>
        <div class="container-fluid bg-dark header-top d-none d-md-block">
            <div class="container">
                <div class="row text-light pt-2 pb-2">
                    <div class="col-md-5"><i class="fa fa-envelope-0" aria-hidden="true" align="center"></i>
                                      PRINCE KHANNA - 16MIN361 BATCH-B SEMESTER-8</div>
                </div>
            </div>
        <div class="container">
        <FORM ID="FIRSTFORM" METHOD="POST" ACTION="">
            <%session.setAttribute("KHANNA", ".");%>
            <TABLE WIDTH="90%" BORDER="1" ALIGN="CENTER" CELLPADDING="3" CELLSPACING="3" RULES="NONE">
                <TR>
                    <TD HEIGHT="60%" COLSPAN="3" ALIGN="CENTER" BGCOLOR="#0E6655"><H1 ><font color="white">DATA LEAKAGE DETECTION OF FRAUD AGENT AND SECURITY USING ENCRYPTION ALGORITHM AND MAC ADDRESS</font></H1>
                        <!--                     //DECLARING THE HEADER FOR THE PROJECT-->
                </TR>
                <TR>
                    <TD WIDTH="144" HEIGHT="300" ALIGN="CENTER" VALIGN="top" BGCOLOR="#EEEEEE">

                        <P><A HREF="AGENT_NEW_USER_REGISTERATION.jsp" class="button">NEW_AGENT_REGISTERATION</A></P>
                        <p><A HREF="CREATE_MAIN_OWNER_LOGIN_PAGE.jsp" class="button">DISTRIBUTOR-LOGIN</A></P>
                        <p><A HREF="AGENT_USER_LOGIN.jsp" class="button">AGENT-LOGIN</A></P>
                    </TD>
                </TR>
            </TABLE>
        </FORM>
    </div>
        </div>
    </body>
</html>

