<%-- 
    Document   : LETS_SEE_WHO_SHARED_REPORTS_TO_THIRD_PERSON
    Created on : Jan 26, 2020, 11:55:39 AM
    Author     : welcome
--%>

<%--<%@page import="java.lang.System.out"%>--%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="DESIGN.css">
        <link rel="stylesheet" href="STYLE.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>DATA LEAKAGE DETECTION OF FRAUD AGENTS AND SECURITY USING ENCRYPTION ALGORITHM MAC ADDRESS</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <%@ include file="DATABASE_CONNECTION_FILE.jsp"%>
    <body>
        <form id="Form1" action="" method="post">
            <TABLE WIDTH="90%" BORDER="1" ALIGN="CENTER" CELLPADDING="3" CELLSPACING="3" RULES="NONE">
                <TR>
                    <TD HEIGHT="60%" COLSPAN="3" ALIGN="CENTER" BGCOLOR="#0E6655"><H1><font color="white">DATA LEAKAGE DETECTION OF FRAUD AGENT AND SECURITY USING ENCRYPTION ALGORITHM AND MAC ADDRESS</font></H1>
                        <!--                     //DECLARING THE HEADER FOR THE PROJECT-->
                </TR>
            </table>
            <br>
            <table width="704" align="center" border="1" id="SAVE_AGNT_USER_DATA_1">
                <tr bgcolor="#FFFFFF" >
                <center><label style="font-size:25px; color:#008B8B">HERE ARE THE USER DETAILS WHO SHARED THE DATA TO THE THIRD OR UNKNOWN PERSON</label></center>
                <br>
                <td width="123" align="center"><font color="#800000" size="2">USER OR AGENT NAME</font></td>
                <td width="89" align="center" ><font color="#800000" size="2">REPORT NAME</font></td>
                <td width="89" align="center" ><font color="#800000" size="2">REPORT-ID</font></td>
                <td width="89" align="center" ><font color="#800000" size="2">REPORT TRANSFER DATE</font></td>
                <td width="101" align="center" ><font color="#800000" size="2">DATA RECEIVER PERSON NAME</font></td>
                <td width="101" align="center" ><font color="#800000" size="2">USER SYSTEM MAC ADDRESS</font></td>
                </tr>
                <jsp:scriptlet>
                    try{
                        String GET_STATUS = "0";
                        PreparedStatement ps=con.prepareStatement("SELECT * FROM GOKH.PRIN_MTECH_SEND_DATA_FROM_AGNT_TO_DISCC where FIXTH_FILE_TRANS_STATUS ='"+GET_STATUS+"'");
                        ResultSet rs=ps.executeQuery();
                        while (rs.next())
                        {
                </jsp:scriptlet>
                <tr bgcolor="#FFFFFF">
                    <td height="20" align="center"><strong><em><font color="#002851"> <%=rs.getString(1)%></font></em></strong></td>
                    <td align="center"><strong><em><font color="#002851"><%=rs.getString(2)%></font></em></strong></td>
                    <td align="center"><strong><em><font color="#002851"><%=rs.getString(3)%></font></em></strong></td>
                    <td align="center"><strong><em><font color="#002851"><%=rs.getString(5)%></font></em></strong></td>
                    <td align="center"><strong><em><font color="#002851"><%=rs.getString(7)%></font></em></strong></td>
                    <td align="center"><strong><em><font color="#002851"><%=rs.getString(8)%></font></em></strong></td>
                </tr>
                <jsp:scriptlet>
                        }
                    }catch (Exception EX){
                        out.println(EX.getMessage());
                    }
                </jsp:scriptlet>
            </table>
                <table width="704" align="center" border="1" id="SAVE_AGNT_USER_DATA_1">
                <tr bgcolor="#FFFFFF" >
                <center><label style="font-size:25px; color:#008B8B">HERE ARE THE TOTAL COUNT FOR REPORT SHARED BETWEEN OWNER AND USER OR AGNET</label></center>
                <br>
                <td width="123" align="center"><font color="#800000" size="2">OWNER - TO - USER</font></td>
                <td width="89" align="center" ><font color="#800000" size="2">REPORT RECIEVED FROM OWNER</font></td>
                </tr>
                <jsp:scriptlet>
                    try{
                        PreparedStatement PRSSTATEMT=con.prepareStatement("SELECT * FROM GOKH.PRIN_MTECH_REPORT_SHARING_DETAILS");
                        ResultSet rs=PRSSTATEMT.executeQuery();
                        while (rs.next()){
                </jsp:scriptlet>
                <tr bgcolor="#FFFFFF">
                    <td height="20" align="center"><strong><em><font color="#002851"> <%=rs.getString(1)%></font></em></strong></td>
                    <td align="center"><strong><em><font color="#002851"><%=rs.getString(2)%></font></em></strong></td>
                </tr>
                <jsp:scriptlet>
                        }
                    }catch (Exception EX){
                        out.println(EX.getMessage());
                    }
                </jsp:scriptlet>
            </table>
        </form>
    </body>
</html>
