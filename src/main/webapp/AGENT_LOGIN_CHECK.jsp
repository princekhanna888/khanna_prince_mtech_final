<%-- 
    Document   : AGENT_LOGIN_CHECK
    Created on : Jan 6, 2020, 6:02:01 AM
    Author     : M15309
--%>
<!--/**
 *
 * @author Prince Khanna
 * @VIT ID 16MIN0361
 * @Wipro ID PR335600
 * @EmailID Prince.khanna@wipro.com
 * @Mobile No 8882071901
 */-->
<%@ page errorPage = "HERE_HANDLING_ALL_RUNTIME_EXCEPTION.jsp" %>
<%@page import="SRC.COM.KHANNA.PRINCE.MTECH.LETS_ENCRYPT_AGNET_USER_PASSWORD_AT_DB_LEVEL"%>
<%@page import="javax.crypto.spec.SecretKeySpec"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="SRC.COM.KHANNA.PRINCE.MTECH.LETS_DECRYPT"%>
<link href="DESIGN_CODE_CSS/SECONDSTYLE.css" rel="stylesheet" type="text/css"/>
<link href="DESIGN_CODE_CSS/SECONDSTYLE.css" rel="stylesheet" type="text/css"/>
<link href="DESIGN_CODE_CSS/FIRSTSTYLE.css" rel="stylesheet" type="text/css"/>
<a href="LETS_CHECK_REGISTERATION_VALUES.jsp"></a>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>KHANNA_PRINCE_MTECH_16MIN0361</title>
    </head>
    <%@ include file="DATABASE_CONNECTION_FILE.jsp"%>
    <body>
        <%            
            String AGENT__USER_USERNAME = request.getParameter("AGENT_USER_NAME");
            final String AGENT__USER_PASSWORD = request.getParameter("AGENTS_PASSWORD");
            session.setAttribute("AGENT_USER_NAME", AGENT__USER_USERNAME);
            session.setAttribute("AGENTS_PASSWORD", AGENT__USER_PASSWORD);
            String USER1,GET_DECRYPTED_VALUE = null,LETS_PUT_KEY = "KHANNAPRINCE4711";
            try {
                PreparedStatement PREPAREDSTATEMENT = con.prepareStatement("SELECT * FROM GOKH.PRIN_MTECH_REGISTERATION_SIGN_UP WHERE AGNT_USER_NAME ='" + AGENT__USER_USERNAME + "'");
                ResultSet RESULTSET = PREPAREDSTATEMENT.executeQuery();
                while (RESULTSET.next()) {
                    GET_DECRYPTED_VALUE = RESULTSET.getString(6);
                }
                //-----------------------------------------------------------------------//
//                IN BELOW CODE WE ARE NOW GOING TO ADD THE AES (ADVANCED ENCRYPTION STANDARD) ALGORITHM
//                        LOGIC WHICH WILL DEPCRYPT THE ENCRYPTED PASSWORD AND IT WILL MATCH 
//                                WITH THE PASSWORD WHICH USER/AGENT IS GOING TO PROVIDE AT THE TIME OF LOGIN 
                SecretKeySpec keySpec = new SecretKeySpec(LETS_PUT_KEY.getBytes(),"AES");
                LETS_ENCRYPT_AGNET_USER_PASSWORD_AT_DB_LEVEL HERE_OUR_LOGIC_FOR_ENC = new LETS_ENCRYPT_AGNET_USER_PASSWORD_AT_DB_LEVEL();
                LETS_DECRYPT HERE_OUR_LOGIC_FOR_DEC = new LETS_DECRYPT();
                String SAVE_DECRYPT = HERE_OUR_LOGIC_FOR_DEC.LETS_HERE_DECRY_OUR_TEXT(GET_DECRYPTED_VALUE, keySpec);
                if (SAVE_DECRYPT.equalsIgnoreCase(AGENT__USER_PASSWORD)){
                    out.println("<script type=\"text/javascript\">");
                    out.println("alert('USER OR AGENT SUCCESSFULLY LOGIN!');");
                    out.println("location='AGENT_USER_SCREEN_PAGE.jsp';");
                    out.println("</script>");
                } else {
                    out.println("<script type=\"text/javascript\">");
                    out.println("alert('USERNAME OR PASSWORD IS INCORRECT!');");
                    out.println("location='AGENT_USER_LOGIN.jsp';");
                    out.println("</script>");
                }
            }catch (Exception ex) {
                throw new RuntimeException("Error condition!!!");            }
        %>
    </body>
</html>
