<%-- 
    Document   : LETS_EXTRACT_USER_REPORTS_FINAL
    Created on : Jan 21, 2020, 1:27:37 AM
    Author     : M15309
--%>

<%@page import="java.io.OutputStream"%>
<%@page import="java.sql.Blob"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="DESIGN.css">
        <link rel="stylesheet" href="STYLE.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>DATA LEAKAGE DETECTION OF FRAUD AGENTS AND SECURITY USING ENCRYPTION ALGORITHM MAC ADDRESS</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <%@ include file="DATABASE_CONNECTION_FILE.jsp"%>
    <body>     
        <jsp:scriptlet>
            int GET_ITH_VALUE = 0;
            String LETS_SAVE_FILE_ID = request.getParameter("value");
            String LETS_SAVE_UNIQUE_CODE = session.getAttribute("LETS_SAVE_UNIQUE_CODE1").toString();
//            out.print(LETS_SAVE_UNIQUE_CODE);
            String LETS_SAVE_REPORT_NAME = null;
//            In the below code we are going to validate whether the key which we are passing to 
//                    DB is matached with the key which we retrieveing from the DB.
            ResultSet SAVE_RESULT = Statement.executeQuery("select * from GOKH.PRIN_MTECH_SEND_DATA_FROM_MAIN_OWNER_TO_AGNT where SECOND_FILE_TYPE_ID = '"+LETS_SAVE_FILE_ID+"' and SEVENTH_UNIQUE_KEY = '"+LETS_SAVE_UNIQUE_CODE+"'");
            //out.print(SAVE_RESULT);
            while (SAVE_RESULT.next()){
                GET_ITH_VALUE++;
                
            }
            if (GET_ITH_VALUE > 0){
                ResultSet SAVE_RESULTS = Statement.executeQuery("select * from GOKH.PRIN_MTECH_REPORT_SHARING_DETAILS");
                int USER_DETAILS_WHO_SHARED = 0;
                while (SAVE_RESULTS.next()){
                    USER_DETAILS_WHO_SHARED = SAVE_RESULTS.getInt(2);
                }
                USER_DETAILS_WHO_SHARED ++;
                int NOW_SAVE_DETAILS_SHARED_DETAILS = Statement.executeUpdate("UPDATE GOKH.PRIN_MTECH_REPORT_SHARING_DETAILS SET USER_REPORT_SHARING_DETAILS = '"+USER_DETAILS_WHO_SHARED+"'");
                try {
                    PreparedStatement PS = con.prepareStatement("select SECOND_FILE_TYPE_ID from GOKH.PRIN_MTECH_SEND_DATA_FROM_MAIN_OWNER_TO_AGNT WHERE SECOND_FILE_TYPE_ID = '"+LETS_SAVE_FILE_ID+"'");
                    ResultSet SAVE_RESULTS_NEXT = PS.executeQuery();
                    while (SAVE_RESULTS_NEXT.next()){
                        LETS_SAVE_REPORT_NAME = SAVE_RESULTS_NEXT.getString("SECOND_FILE_TYPE_ID");
                    }
                }catch (Exception EX){
                    out.println(EX.getMessage());
                }
                Blob GET_FILE_DOWN = null;
                PreparedStatement PREAPERD_STATE = null;
                ResultSet RESULTSET = null;
                String NOW_CREATE_STATEMENT = null;
                NOW_CREATE_STATEMENT = "SELECT FOURTH_FILE_PATH FROM GOKH.PRIN_MTECH_SEND_DATA_FROM_MAIN_OWNER_TO_AGNT WHERE SECOND_FILE_TYPE_ID = '"+LETS_SAVE_FILE_ID+"'";
                try{
                    PREAPERD_STATE = con.prepareStatement(NOW_CREATE_STATEMENT);
                    RESULTSET = PREAPERD_STATE.executeQuery();
                    RESULTSET.next();
                    GET_FILE_DOWN = RESULTSET.getBlob(1);
                    session.setAttribute("LETS_SAVE_BLOB", GET_FILE_DOWN);
                    }catch (Exception EX){
                    out.println("Exception :"+ EX);
                    }
                try{
                    Blob b = (Blob)session.getAttribute("LETS_SAVE_BLOB");
                    if(b != null)
                    {
                            String filename =null;
                            filename+=".docx";
                            byte[] ba = b.getBytes(1, (int)b.length());
                            response.setContentType("application/msword");
                            response.setHeader("Content-Disposition","attachment; filename=\""+LETS_SAVE_REPORT_NAME+"\"");
                            OutputStream os = response.getOutputStream();
                            os.write(ba);
                            os.close();
                            ba = null;
                            session.removeAttribute("budget");
                    }
		}catch (Exception EX){
                            EX.getStackTrace();
                }
            }
        </jsp:scriptlet>
        
        
    </body>
</html>
