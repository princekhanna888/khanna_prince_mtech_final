<%-- 
    Document   : VALID_USER_OR_AGNTS_DETAILS
    Created on : Jan 17, 2020, 12:33:51 AM
    Author     : M15309
--%>

<%@page import="com.sun.media.jfxmedia.logging.Logger"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link href="DESIGN_CODE_CSS/FIRSTSTYLE.css" rel="stylesheet" type="text/css"/>
        <link href="DESIGN_CODE_CSS/SECONDSTYLE.css" rel="stylesheet" type="text/css"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>DATA LEAKAGE DETECTION OF FRAUD AGENTS AND SECURITY USING ENCRYPTION ALGORITHM MAC ADDRESS</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <%@ include file="DATABASE_CONNECTION_FILE.jsp"%>
    <body>
        <form id="Form1" action="" method="post">
            <TABLE WIDTH="90%" BORDER="1" ALIGN="CENTER" CELLPADDING="3" CELLSPACING="3" RULES="NONE">
                <TR>
                    <TD HEIGHT="60%" COLSPAN="3" ALIGN="CENTER" BGCOLOR="#0E6655"><H1><font color="white">DATA LEAKAGE DETECTION OF FRAUD AGENT AND SECURITY USING ENCRYPTION ALGORITHM AND MAC ADDRESS</font></H1>
                        <!--                     //DECLARING THE HEADER FOR THE PROJECT-->
                </TR>
            </table>
            </br>
            <table width="704" align="center" border="1" id="SAVE_AGNT_USER_DATA">
                <tr bgcolor="#FFFFFF" >
                    <td width="166" height="21" align="center"><font color="#800000" size="2">VALID_USER_OR_AGNT</font></td>
                    <td width="123" align="center"><font color="#800000" size="2">VALID_EMAIL_ID</font></td>
                    <td width="89" align="center" ><font color="#800000" size="2">VALID_MOBILE_NO</font></td>
                    <td width="101" align="center" ><font color="#800000" size="2">VALID_ADDRESS</font></td>
                </tr>
                <jsp:scriptlet>                    try {
                        PreparedStatement PREAPERD_STATE = con.prepareStatement("SELECT * FROM GOKH.PRIN_MTECH_REGISTERATION_SIGN_UP");
                        ResultSet RESULTSET = PREAPERD_STATE.executeQuery();
                        while (RESULTSET.next()) {
                </jsp:scriptlet> 
                <tr bgcolor="#FFFFFF">
                    <td height="20" align="center"><strong><em><font color="#002851"> <%=RESULTSET.getString(1)%></font></em></strong></td>
                    <td align="center"><strong><em><font color="#002851"><%=RESULTSET.getString(3)%></font></em></strong></td>
                    <td align="center"><strong><em><font color="#002851"><%=RESULTSET.getString(4)%></font></em></strong></td>
                    <td align="center"><strong><em><font color="#002851"><%=RESULTSET.getString(5)%></font></em></strong></td>
                </tr>
                <jsp:scriptlet>
                        }
                    } catch (Exception EX) {
                        EX.getStackTrace();
                    }
                </jsp:scriptlet>
            </table>
            <center><input style="color:white; margin:20px 100px;background-color:#0E6655;height: 40px;width: 450px" type="button" id="SAVE_VALID_USER_DATA" value="EXTRACT REPORT FOR AUTHORIZED AGNT OR USER" align="center" />
            <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.22/pdfmake.min.js"></script>
            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>
            <script type="text/javascript">
                $("body").on("click", "#SAVE_VALID_USER_DATA", function () {
                    html2canvas($('#SAVE_AGNT_USER_DATA')[0], {
                        onrendered: function (canvas) {
                            var data = canvas.toDataURL();
                            var docDefinition = {
                                content: [{
                                        image: data,
                                        width: 500
                                    }]
                            };
                            pdfMake.createPdf(docDefinition).download("GET DATA.pdf");
                        }
                    });
                });
            </script>
        </form>
    </body>
</html>
