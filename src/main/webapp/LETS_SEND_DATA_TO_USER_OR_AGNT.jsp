<%-- 
    Document   : LETS_SEND_DATA_TO_USER_OR_AGNT
    Created on : Jan 17, 2020, 3:44:51 AM
    Author     : M15309
--%>

<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link href="DESIGN_CODE_CSS/FIRSTSTYLE.css" rel="stylesheet" type="text/css"/>
        <link href="DESIGN_CODE_CSS/SECONDSTYLE.css" rel="stylesheet" type="text/css"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>DATA LEAKAGE DETECTION OF FRAUD AGENTS AND SECURITY USING ENCRYPTION ALGORITHM MAC ADDRESS</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <%@ include file="DATABASE_CONNECTION_FILE.jsp"%>
    <body>
        <form id="Form1" action="LETS_DO_THE_INSERTION_RECORDS_FROM_OWNER_TO_USER.jsp" method="post">
            <TABLE WIDTH="90%" BORDER="1" ALIGN="CENTER" CELLPADDING="3" CELLSPACING="3" RULES="NONE">
                <TR>
                    <TD HEIGHT="60%" COLSPAN="3" ALIGN="CENTER" BGCOLOR="#0E6655"><H1><font color="white">DATA LEAKAGE DETECTION OF FRAUD AGENT AND SECURITY USING ENCRYPTION ALGORITHM AND MAC ADDRESS</font></H1>
                        <!--                     //DECLARING THE HEADER FOR THE PROJECT-->
                </TR>
            </table>
            <div style="height:495px; width:500px; background-color:#E5E7E9; margin: 0 auto; margin-top: 40px;">
                <label style="font-size:25px; color:#008B8B" align="center">TO</label>
                <right><select name="HERE_COMES_THE_VALID_USER" class="GET_DETAILS_ABOUT_AGNT" id="GET_DATA_FROM_DB" align="center">
                    <option>SELECT THE VALID USER OR AGNT</option>
                    <jsp:scriptlet>
                        String GET_USER_DETAILS_FROM_DB = "SELECT * FROM GOKH.PRIN_MTECH_REGISTERATION_SIGN_UP";
                        ResultSet GET_RESULT=Statement.executeQuery(GET_USER_DETAILS_FROM_DB);
                        while (GET_RESULT.next()){
                    </jsp:scriptlet>
                    <option> <%=GET_RESULT.getString(1)%> </option>
                    <jsp:scriptlet>
                        }
                    </jsp:scriptlet>
                </select></right>
            <br>
            <label style="font-size:25px; color:#008B8B">FILE-NAME</label>
            <input type="TEXT" placeholder="ENTER THE FILE-NAME" class="PLEASE ENTER THE FILE NAME" name="FILE-NAME" value="" required>
            <br>
            <label style="font-size:25px; color:#008B8B">FILE-ID</label>
            <input type="text" placeholder="" class="HERE ID IS RANDOMALY GENERATING" name="FILE-ID" value=" <%= (int) (Math.random() * 5000 * 10000)%>" required>
            <br>
            <label style="font-size:25px; color:#008B8B">CHOOSE-FILE</label>
            <input type="FILE" class="PLEASE CHOOSE THE FILE WHICH NEEDS TO SEND" name="CHOOSE-FILE" required>
            <br>
            <center><input style="color:white; margin:20px 100px;background-color:#0E6655;height: 40px;width: 300px" type="submit" name="SEND DATA TO AGENTS OR VALID USER" value="SEND DATA TO AGENTS OR VALID USER"></center>
            <br>
        </div>
    </form>
</body>
</html>
