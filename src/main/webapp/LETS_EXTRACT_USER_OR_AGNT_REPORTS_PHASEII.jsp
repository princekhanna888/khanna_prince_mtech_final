<%-- 
    Document   : LETS_EXTRACT_USER_OR_AGNT_REPORTS_PHASEII
    Created on : Jan 19, 2020, 11:09:07 PM
    Author     : welcome
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="DESIGN.css">
        <link rel="stylesheet" href="STYLE.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>DATA LEAKAGE DETECTION OF FRAUD AGENTS AND SECURITY USING ENCRYPTION ALGORITHM MAC ADDRESS</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <%@ include file="DATABASE_CONNECTION_FILE.jsp"%>
    <body>
        <form id="Form1" action="" method="post">
            <TABLE WIDTH="90%" BORDER="1" ALIGN="CENTER" CELLPADDING="3" CELLSPACING="3" RULES="NONE">
                <TR>
                    <TD HEIGHT="60%" COLSPAN="3" ALIGN="CENTER" BGCOLOR="#0E6655"><H1><font color="white">DATA LEAKAGE DETECTION OF FRAUD AGENT AND SECURITY USING ENCRYPTION ALGORITHM AND MAC ADDRESS</font></H1>
                        <!--                     //DECLARING THE HEADER FOR THE PROJECT-->
                </TR>
            </table>
            <br>
            <table width="704" align="center" border="1" id="SAVE_AGNT_USER_DATA_1">
                <tr bgcolor="#FFFFFF" >
                <label style="font-size:25px; color:#008B8B">PLEASE ENTER THE UNIQUE-KEY</label>
                <br>
                <input type="text"  placeholder="ENTER THE UNIQUE-KEY" name="UNIQUE-KEY" maxlength="10" size="50" value="ENTER THE UNIQUE-CODE AND PRESS ENTER" required>
                <br>
                <br>
                <td width="123" align="center"><font color="#800000" size="2">REPORT OR FILE NAME</font></td>
                <td width="89" align="center" ><font color="#800000" size="2">REPORT ID</font></td>
                <td width="101" align="center" ><font color="#800000" size="2">REPORT TRANSFER DATE</font></td>
                <td width="101" align="center" ><font color="#800000" size="2">REPORT DOWNLOAD</font></td>
                    <jsp:scriptlet>                        
                        String SAVE_UNIQUE_CODE = request.getParameter("UNIQUE-KEY");
                        session.setAttribute("LETS_SAVE_UNIQUE_CODE1", SAVE_UNIQUE_CODE);
                        String SAVE_FILE_ID = request.getParameter("value");
                        try {
                            //String SQL = "SELECT * FROM GOKH.PRIN_MTECH_SEND_DATA_FROM_MAIN_OWNER_TO_AGNT WHERE SECOND_FILE_TYPE_ID = '" + SAVE_FILE_ID + "'";
                            PreparedStatement PS = con.prepareStatement("SELECT * FROM GOKH.PRIN_MTECH_SEND_DATA_FROM_MAIN_OWNER_TO_AGNT WHERE SECOND_FILE_TYPE_ID = '" + SAVE_FILE_ID + "'");
                            ResultSet RESULTSET = PS.executeQuery();
                            //out.print(RESULTSET);
                            String SAVE_FILE_NAME = null;
                            while (RESULTSET.next()) {
                                SAVE_FILE_NAME = RESULTSET.getString(3);
                                session.setAttribute("SAVE_NAME", SAVE_FILE_NAME);
                    </jsp:scriptlet>
                <tr bgcolor="#FFFFFF">
                    <td height="20" align="center"><strong><em><font color="#002851"> <%=RESULTSET.getString(3)%></font></em></strong></td>
                    <td align="center"><strong><em><font color="#002851"><%=RESULTSET.getString(2)%></font></em></strong></td>
                    <td align="center"><strong><em><font color="#002851"><%=RESULTSET.getString(5)%></font></em></strong></td>
                    <td align="center"><strong><em><font color="#002851"><a href="LETS_EXTRACT_USER_REPORTS_FINAL.jsp?value=<%=RESULTSET.getString(2)%>"></font></em></strong><blink>TO DOWNLOAD THIS FILE CLICK HERE !</blink></font></a></td>
                </tr>
                <jsp:scriptlet>
                        }
                    } catch (Exception EX){
                        out.println(EX.getMessage());
                    }
                </jsp:scriptlet>
                </tr>
            </table>
        </form>
    </body>
</html>
