<%-- 
    Document   : LETS_DO_THE_INSERTION_RECORDS_FROM_OWNER_TO_USER
    Created on : Jan 17, 2020, 4:24:09 AM
    Author     : M15309
--%>

<%@page import="java.io.IOException"%>
<%@page import="java.io.FileOutputStream"%>
<%@page import="java.awt.Color"%>
<%@page import="java.awt.Graphics"%>
<%@page import="java.awt.image.BufferedImage"%>
<%@page import="javax.imageio.ImageIO"%>
<%@page import="java.awt.Image"%>
<%@page import="java.awt.AlphaComposite"%>
<%@page import="java.awt.Font"%>
<%@page import="java.awt.Graphics2D"%>
<%@page import="javax.swing.ImageIcon"%>
<%@ page errorPage = "HERE_HANDLING_ALL_RUNTIME_EXCEPTION.jsp" %>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.io.InputStream"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="java.io.File"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="DESIGN.css">
        <link rel="stylesheet" href="STYLE.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>DATA LEAKAGE DETECTION OF FRAUD AGENTS AND SECURITY USING ENCRYPTION ALGORITHM MAC ADDRESS</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <%@ include file="DATABASE_CONNECTION_FILE.jsp"%>
    <body>
        <TABLE WIDTH="90%" BORDER="1" ALIGN="CENTER" CELLPADDING="3" CELLSPACING="3" RULES="NONE">
            <TR>
                <TD HEIGHT="60%" COLSPAN="3" ALIGN="CENTER" BGCOLOR="#0E6655"><H1><font color="white">DATA LEAKAGE DETECTION OF FRAUD AGENT AND SECURITY USING ENCRYPTION ALGORITHM AND MAC ADDRESS</font></H1>
                    <!--                     //DECLARING THE HEADER FOR THE PROJECT-->
            </TR>
        </table>
        <%            int UNIQUE_OTP = (int) (Math.random() * 10000000 - 10000);
            session.setAttribute("OTP", UNIQUE_OTP);
            String SAVE_OTP = session.getAttribute("OTP").toString();
//            NOW WE ARE GOING TO SEND THIS RANDOM OTP VALUE TO THE VALID USER AND THIS SAME OTP FURTHER 
//                    THEY WILL USE TO DOWNLOAD OR VIEW THE FILES WHICH OWNER IS GOING TO SHARE 
            int GET_STATUS = 1;
            String DATE_FORMAT = "yyyy-MM-dd";
            // IN BELOW CODE WE ARE ADDING THE LOGIC FOR THE SYSTEM DATE
            java.util.Date TODAYS_DATE = new java.util.Date();
            SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat(DATE_FORMAT);
            String USER_NAME = request.getParameter("HERE_COMES_THE_VALID_USER");
            System.out.println(USER_NAME);
            String DATE_STANDARD = SIMPLE_DATE_FORMAT.format(TODAYS_DATE);
            //            NOW IN THE BELOW CODE WE ARE ADDING THE LOGIC FOR GETTING THE FILENAME,FILE-ID AND FILE-PATH VALUE TO INSERT INTO THE DATABASE 
            String FILE_NAME = request.getParameter("FILE-NAME");
            String TAKE_FILE_ID = request.getParameter("FILE-ID");
            //session.getAttribute(TAKE_FILE_ID);
            String FILE_ONE = request.getParameter("CHOOSE-FILE");
            String FILE_PATH = "C:\\Program Files\\Apache Software Foundation\\Tomcat 8.5\\webapps\\KHANNA_PRINCE_16MIN0361_8SEM_MTECH_PROJECT\\Data\\";
            String SLECT_FILE = FILE_PATH + FILE_ONE;
            File ff = new File(SLECT_FILE);
            double size = ff.length();
            double size1 = size / 1024;
            double size2 = Math.round(size1 * 100.0) / 100.0;
            String size3 = Double.toString(size2);
            session.setAttribute("filesize", size3);
            String PRINCE = "0";
            String KHNNA = "0";
            FileInputStream FILE_INPUT_STREAM;
//            NOW IN THE BELOW CODE I AM GOING TO ADD THE WATERMARK FUNCTIONALITY WHICH WILL HELP
//                    ANY TOANY PEOPLE TO NOT TO SEND THE REAL DATA THEY CAN ADD OR
//                            CREATE DIFFERENT COPY FOR THAT DOCUMENTS.
//            File origFile = new File(SLECT_FILE);
//            ImageIcon icon = new ImageIcon(origFile.getPath());
//            // create BufferedImage object of same width and height as of original image
//            BufferedImage bufferedImage = new BufferedImage(icon.getIconWidth(),
//                    icon.getIconHeight(), BufferedImage.TYPE_INT_RGB);
//            // create graphics object and add original image to it
//            Graphics graphics = bufferedImage.getGraphics();
//            graphics.drawImage(icon.getImage(), 0, 0, null);
//            // set font for the watermark text
//            graphics.setFont(new Font("Arial", Font.BOLD, 30));
//            //unicode characters for (c) is \u00a9
//            String watermark = "\u00a9 JavaXp.com";
//            // add the watermark text
//            graphics.drawString(watermark, 0, icon.getIconHeight() / 2);
//            graphics.dispose();
//            File newFile = new File("C:\\WatermarkedImage.jpg");
//            try {
//                  ImageIO.write(bufferedImage, "jpg", newFile);
//            } catch (IOException e) {
//                  e.printStackTrace();
//            }
//            IN THE BELOW CODE WE ARE GOING TO ADD LOGIC TO INSERT AGENT NAME WHO IS GOING TO SEND THE FILE TO FRAUD AGENTS AS WELL AS 
//                    DISTRIBUTOR WITH THERE MC ADDRESS AND FILE NAME , FILE ID , FILE 

            try {
                String STATUS_ONE = String.valueOf(GET_STATUS);
                PreparedStatement ps = con.prepareStatement("insert into GOKH.PRIN_MTECH_SEND_DATA_FROM_MAIN_OWNER_TO_AGNT(FIRST_AGNT_USER_NAME,SECOND_FILE_TYPE_ID,THIRD_FILE_NAME,FOURTH_FILE_PATH,FIFTH_DATE_TRANS,FIXTH_FILE_TRANS_STATUS,SEVENTH_UNIQUE_KEY) values (?,?,?,?,?,?,?)");
                FILE_INPUT_STREAM = new FileInputStream(ff);
                ps.setString(1, USER_NAME);
                ps.setString(3, FILE_NAME);
                ps.setString(2, TAKE_FILE_ID);
                ps.setBinaryStream(4, (InputStream) FILE_INPUT_STREAM, (int) (ff.length()));
                ps.setString(5, DATE_STANDARD);
                ps.setString(6, STATUS_ONE);
                ps.setString(7, SAVE_OTP);
                ps.executeUpdate();
                ResultSet res = Statement.executeQuery("select * from GOKH.PRIN_MTECH_REPORT_SHARING_DETAILS");
                int distributor = 0;
                while (res.next()) {
                    distributor = res.getInt(1);
                }
                distributor++;
                int in = Statement.executeUpdate("update  GOKH.PRIN_MTECH_REPORT_SHARING_DETAILS  set ONWER_REPORT_SHARING_DETAILS ='" + distributor + "'");
                out.println("<script type=\"text/javascript\">");
                out.println("alert(\"DATA SENT SUCESSFULLY TO !" + request.getParameter("HERE_COMES_THE_VALID_USER") + "\")");
                out.println("location='OWNER_DISTRI_SCREEN_PAGE.jsp';");
                out.println("</script>");
            } catch (Exception e) {
                throw new RuntimeException("Error condition!!!");
            }
        %>
    </body>
</html>
