<%-- 
    Document   : LETS_CHECK_WHTHER_DATA_SEND_TO_DISTRIBUTOR
    Created on : Jan 16, 2020, 4:13:32 AM
    Author     : M15309
--%>

<%@page import="java.io.InputStream"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="java.io.File"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <%@ include file="DATABASE_CONNECTION_FILE.jsp"%>
    <body>
<!--        String TAKE_FILE_NAME = ${param.FILE-NAME};
        String TAKE_FILE_ID = ${param.FILE_ID};-->

        <%            
            int GET_STATUS = 0;
            String DATE_FORMAT = "yyyy-MM-dd";
            // IN BELOW CODE WE ARE ADDING THE LOGIC FOR THE SYSTEM DATE
            java.util.Date TODAYS_DATE = new java.util.Date();
            SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat(DATE_FORMAT);
            String USER_NAME = (String) session.getAttribute("AGENT_USER_NAME");
            String DATE_STANDARD = SIMPLE_DATE_FORMAT.format(TODAYS_DATE);
            String RECIPIENT_NAME = request.getParameter("TO");
            try {
                PreparedStatement PREPAREDSTATEMENT = con.prepareStatement("select * from GOKH.MAIN_OWNER_TABLE mot , GOKH.PRIN_MTECH_REGISTERATION_SIGN_UP gpmrsu where gpmrsu.AGNT_USER_NAME='" + RECIPIENT_NAME + "' or mot.OWNER_USER_NAME='" + RECIPIENT_NAME + "'");
                ResultSet RESULTSET = PREPAREDSTATEMENT.executeQuery();
                int LETS_INSRT_VALUE = 0;
                while (RESULTSET.next()) {
                    LETS_INSRT_VALUE++;
                } if (LETS_INSRT_VALUE != 0) {
                    GET_STATUS = 1;
                }
            } catch (Exception EX) {
                EX.getStackTrace();
            }
            //            NOW IN THE BELOW CODE WE ARE ADDING THE LOGIC FOR GETTING THE FILENAME,FILE-ID AND FILE-PATH VALUE TO INSERT INTO THE DATABASE 
            String FILE_NAME = request.getParameter("FILE-NAME");
            String TAKE_FILE_ID = request.getParameter("FILE-ID");
            String FILE_ONE = request.getParameter("CHOOSE-FILE");
//            ServletContext context = pageContext.getServletContext();
            String FILE_PATH = "C:\\Program Files\\Apache Software Foundation\\Tomcat 8.5\\webapps\\KHANNA_PRINCE_16MIN0361_8SEM_MTECH_PROJECT\\Data\\";
            String SLECT_FILE = FILE_PATH + FILE_ONE;
            File ff = new File(SLECT_FILE);
            double size = ff.length();
            double size1 = size / 1024;
            double size2 = Math.round(size1 * 100.0) / 100.0;
            String size3 = Double.toString(size2);
            session.setAttribute("filesize", size3);
            String PRINCE = "0";
            String KHNNA = "0";
            FileInputStream FILE_INPUT_STREAM;
            //            NOW IN THE BELOW CODE WE ARE GOING TO ADD THE MAC ADDRESS LOGIC TO GET THE USER OR AGENT MAC ADDRESS
//                WHILE SENDING THE FILE TO ANY UNAUTHORISED USER OR FRAUD AGENTS
            String MAC_ADDRESS = null;
            try {
                Process p = Runtime.getRuntime().exec("getmac /fo csv /nh");
                BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
                String line;
                line = in.readLine();
                String[] result = line.split(",");
                MAC_ADDRESS = result[0].replace('"', ' ').trim();
                out.println(MAC_ADDRESS);
            } catch (Exception EXCEPTION) {
                EXCEPTION.printStackTrace();
            }
            //            IN THE BELOW CODE WE ARE GOING TO ADD LOGIC TO INSERT AGENT NAME WHO IS GOING TO SEND THE FILE TO FRAUD AGENTS AS WELL AS 
//                    DISTRIBUTOR WITH THERE MC ADDRESS AND FILE NAME , FILE ID , FILE 
            try {
                String STATUS_ONE = String.valueOf(GET_STATUS);
                PreparedStatement ps = con.prepareStatement("insert into GOKH.PRIN_MTECH_SEND_DATA_FROM_AGNT_TO_DISCC(FIRST_AGNT_USER_NAME,THIRD_FILE_NAME,SECOND_FILE_TYPE_ID,FOURTH_FILE_PATH,FIFTH_DATE_TRANS,FIXTH_FILE_TRANS_STATUS,SEVENTH_NAME_OF_RECIEVER_PERSON,EIGTH_MACADDRESS) values (?,?,?,?,?,?,?,?)");
                FILE_INPUT_STREAM = new FileInputStream(ff);
                ps.setString(1, USER_NAME);
                ps.setString(2, FILE_NAME);
                ps.setString(3, TAKE_FILE_ID);
                ps.setBinaryStream(4, (InputStream) FILE_INPUT_STREAM, (int) (ff.length()));
                ps.setString(5, DATE_STANDARD);
                ps.setString(6, STATUS_ONE);
                ps.setString(7, RECIPIENT_NAME);
                ps.setString(8, MAC_ADDRESS);
                ps.executeUpdate();
                ResultSet res=Statement.executeQuery("select * from GOKH.PRIN_MTECH_REPORT_SHARING_DETAILS");
                int USER=0;
                while(res.next())
                {
                    USER=res.getInt(2);
                }
                USER++;
                int in=Statement.executeUpdate("update  GOKH.PRIN_MTECH_REPORT_SHARING_DETAILS  set USER_REPORT_SHARING_DETAILS ='"+USER+"'");
                out.println("<script type=\"text/javascript\">");
                out.println("alert('DATA SENT SUCESSFULLY!');");
                out.println("location='AGENT_USER_SCREEN_PAGE.jsp';");
                out.println("</script>");
            } catch (Exception e) {
                out.println(e.getMessage());
            }
        %>
    </body>
</html>
