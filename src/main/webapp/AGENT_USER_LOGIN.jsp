<%-- 
    Document   : AGENT_USER_LOGIN
    Created on : Jan 6, 2020, 4:52:45 AM
    Author     : M15309
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link href="DESIGN_CODE_CSS/FIRSTSTYLE.css" rel="stylesheet" type="text/css"/>
        <link href="DESIGN_CODE_CSS/SECONDSTYLE.css" rel="stylesheet" type="text/css"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>DATA LEAKAGE DETECTION OF FRAUD AGENTS AND SECURITY USING ENCRYPTION ALGORITHM MAC ADDRESS</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>
        <form id="Form1" action="AGENT_LOGIN_CHECK.jsp" method="post">
            <TABLE WIDTH="90%" BORDER="1" ALIGN="CENTER" CELLPADDING="3" CELLSPACING="3" RULES="NONE">
                <TR>
                    <TD HEIGHT="60%" COLSPAN="3" ALIGN="CENTER" BGCOLOR="#0E6655"><H1><font color="white">DATA LEAKAGE DETECTION OF FRAUD AGENT AND SECURITY USING ENCRYPTION ALGORITHM AND MAC ADDRESS</font></H1>
                        <!--                     //DECLARING THE HEADER FOR THE PROJECT-->
                </TR>
            </table>

            <div style="height:395px; width:500px; background-color:#E5E7E9; margin: 0 auto; margin-top: 40px;">
                <label style="font-size:25px; color:#008B8B">USERNAME</label>
                <input type="text"  placeholder="ENTER AGENT-USER-NAME" name="AGENT_USER_NAME" value="" required>
                <br>
                <label style="font-size:25px; color:#008B8B">PASSWORD</label>
                <input type="password" placeholder="ENTER PASSWORD" name="AGENTS_PASSWORD" value="" required>
                <br>
                <center><input style="color:white; margin:20px 100px;background-color:#0E6655;height: 40px;width: 150px" type="submit" name="AGENT_LOGIN" value="AGENTLOGIN"></center>
            </div>
            
        </form>
    </body>
</html>
