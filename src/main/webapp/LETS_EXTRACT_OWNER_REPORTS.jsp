<%-- 
    Document   : LETS_EXTRACT_OWNER_REPORTS
    Created on : Jan 18, 2020, 2:58:10 PM
    Author     : welcome
--%>

<%@page import="java.io.OutputStream"%>
<%@page import="java.sql.Blob"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="DESIGN.css">
        <link rel="stylesheet" href="STYLE.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>DATA LEAKAGE DETECTION OF FRAUD AGENTS AND SECURITY USING ENCRYPTION ALGORITHM MAC ADDRESS</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <%@ include file="DATABASE_CONNECTION_FILE.jsp"%>
    <body>     
        <jsp:scriptlet>
            String LETS_GET_FILE_ID = request.getParameter("value");
            out.print(LETS_GET_FILE_ID);
            String FILE_NAME = null;
            try{
                PreparedStatement PREAPERD_STATE = con.prepareStatement("SELECT SECOND_FILE_TYPE_ID FROM GOKH.PRIN_MTECH_SEND_DATA_FROM_AGNT_TO_DISCC WHERE SECOND_FILE_TYPE_ID = '"+LETS_GET_FILE_ID+"'");
                ResultSet RESULTSET = PREAPERD_STATE.executeQuery();
                if (RESULTSET.next()){
                    FILE_NAME = RESULTSET.getString("SECOND_FILE_TYPE_ID");    
                }
            }catch (Exception EX){
                out.println(EX.getMessage());
            }
            Blob GET_FILE_DOWN = null;
            PreparedStatement PREAPERD_STATE = null;
            ResultSet RESULTSET = null;
            String NOW_CREATE_STATEMENT = null;
            NOW_CREATE_STATEMENT = "SELECT FOURTH_FILE_PATH FROM GOKH.PRIN_MTECH_SEND_DATA_FROM_AGNT_TO_DISCC WHERE SECOND_FILE_TYPE_ID = '"+LETS_GET_FILE_ID+"'";
            try{
                PREAPERD_STATE = con.prepareStatement(NOW_CREATE_STATEMENT);
                RESULTSET = PREAPERD_STATE.executeQuery();
                RESULTSET.next();
                GET_FILE_DOWN = RESULTSET.getBlob(1);
                session.setAttribute("LETS_SAVE_BLOB", GET_FILE_DOWN);
                }catch (Exception EX){
                    out.println("Exception :"+ EX);
                }
            try{
            	Blob b = (Blob)session.getAttribute("LETS_SAVE_BLOB");
                if(b != null)
		{
	
			String filename =null;
			filename+=".docx";
			byte[] ba = b.getBytes(1, (int)b.length());
			response.setContentType("application/msword");
			response.setHeader("Content-Disposition","attachment; filename=\""+FILE_NAME+"\"");
			OutputStream os = response.getOutputStream();
			os.write(ba);
			os.close();
			ba = null;
			session.removeAttribute("budget");
			}
			}catch (Exception EX){
                            EX.getStackTrace();
                        }
        </jsp:scriptlet>
    </body>
</html>
