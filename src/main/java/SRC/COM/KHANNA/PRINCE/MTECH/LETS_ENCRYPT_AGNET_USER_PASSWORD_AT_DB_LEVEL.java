package SRC.COM.KHANNA.PRINCE.MTECH;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import org.apache.log4j.Logger;
import org.apache.log4j.BasicConfigurator;

/**

 * @author Prince Khanna
 * @VIT ID 16MIN0361
 * @Wipro ID PR335600
 * @EmailID Prince.khanna@wipro.com
 * @Mobile No 8882071901
 */
public class LETS_ENCRYPT_AGNET_USER_PASSWORD_AT_DB_LEVEL {
    public static Logger log = Logger.getLogger(RUNNING_FILE.class);
//    String PlainText;
    String LETS_PUT_KEY = "KHANNAPRINCE4711";
    public String LETS_GENERATE_ENCRYPT_VALUES(String PlainText, SecretKeySpec keySpec) {
        BasicConfigurator.configure();
        //we are going to deal with byte arrays
        byte[] GET_OUR_ENCRY_DATA_HERE = null;
        try {
//                    In the below code, I an using Cipher Java class with Cipher instance. This instance is 
//                        going to call the GETINSTANCE method which means it will tell what type of 
//                            format we need to perform the encryption with what type of algo. 
//                                And in below code i used the Algo as AES i.e. Advanced 
//                                                Encryption Standrad
            Cipher HERE_OUR_ENCRYPTED_OBJECT = Cipher.getInstance("AES");
//                    Now next step is that i am going to initiate the Cipher class for this we are going to do by calling
//                        its init method as this method it will be taking two parameter i.e. (So very First one is the 
//                            MODE level and the another one is the KEY which we are going to use for Encryption as
//                                well as at the time of decryption of the particular string.)
            HERE_OUR_ENCRYPTED_OBJECT.init(Cipher.ENCRYPT_MODE, keySpec);
//                        Now in the below code i am going to take the text from the PlainText Parameter or we can say
//                                we will take this value from the user interface and then with that value we are 
//                                    going to convert that text into bytes and we will store in byte[] OUR_MAIN_TEXT
            byte[] OUR_MAIN_TEXT = PlainText.getBytes();
//                        Now here comes the main logic is working means with the help of dofinal method we are going to encry
//                                our PlainText into GET_OUR_ENCRY_DATA_HERE.
            GET_OUR_ENCRY_DATA_HERE = HERE_OUR_ENCRYPTED_OBJECT.doFinal(OUR_MAIN_TEXT);
//                        Now in the below code encode the encry text with the help of base64. This base 64 is coming from the 
//                                import java.util.Base64 class
            System.out.println("DOFINAL TEXT"+ Arrays.toString(GET_OUR_ENCRY_DATA_HERE));
            GET_OUR_ENCRY_DATA_HERE = Base64.getEncoder().encode(GET_OUR_ENCRY_DATA_HERE);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException e) {
            log.error("Here is we found the Exception:------>" + e);
        }
        return new String(GET_OUR_ENCRY_DATA_HERE);
    }
}
