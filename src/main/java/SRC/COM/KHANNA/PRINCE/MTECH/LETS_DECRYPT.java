/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SRC.COM.KHANNA.PRINCE.MTECH;

import static SRC.COM.KHANNA.PRINCE.MTECH.LETS_ENCRYPT_AGNET_USER_PASSWORD_AT_DB_LEVEL.log;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

/**
 *
 * @author Prince Khanna
 * @VIT ID 16MIN0361
 * @Wipro ID PR335600
 * @EmailID Prince.khanna@wipro.com
 * @Mobile No 8882071901
 */
public class LETS_DECRYPT {

    public static Logger log = Logger.getLogger(RUNNING_FILE.class);

    public String LETS_HERE_DECRY_OUR_TEXT(String cipherText, SecretKeySpec keySpec) {
        BasicConfigurator.configure();
        byte[] HERE_COME_DECRY_VALYE = null;
        try {
//                    In the below code, I an using Cipher Java class with Cipher instance. This instance is 
//                        going to call the GETINSTANCE method which means it will tell what type of 
//                            format we need to perform the encryption with what type of algo. 
//                                And in below code i used the Algo as AES i.e. Advanced 
//                                                Encryption Standrad
            Cipher decryptionCipher = Cipher.getInstance("AES");
//                    Now next step is that i am going to initiate the Cipher class for this we are going to do by calling
//                        its init method as this method it will be taking two parameter i.e. (So very First one is the 
//                            MODE level and the another one is the KEY which we are going to use for Encryption as
//                                well as at the time of decryption of the particular string.)            
            decryptionCipher.init(Cipher.DECRYPT_MODE, keySpec);
//                        Now in the below code encode the encry text with the help of base64. This base 64 is coming from the 
//                                import java.util.Base64 class and here Base64 it is an encoding which will represent any 
//                                          byte sequence by a sequence which will be in printable characters           
            byte[] bytedDecrypted = Base64.getDecoder().decode(cipherText.getBytes());
//                        Now here comes the main logic is working means with the help of dofinal method we are going to decry
//                                our cipherText into HERE_COME_DECRY_VALYE.
            HERE_COME_DECRY_VALYE = decryptionCipher.doFinal(bytedDecrypted);
            System.out.println(Arrays.toString(HERE_COME_DECRY_VALYE) + "DOFINAL TEXT");
            return new String(HERE_COME_DECRY_VALYE, "UTF8");
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException | UnsupportedEncodingException ERROR) {
            log.error("Here is we found the Exception:------>" + ERROR);
        }
        return null;
    }
}
