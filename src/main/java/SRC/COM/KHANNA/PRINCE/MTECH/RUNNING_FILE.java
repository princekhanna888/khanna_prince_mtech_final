/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SRC.COM.KHANNA.PRINCE.MTECH;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import java.security.NoSuchAlgorithmException;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author M15309
 */
public class RUNNING_FILE {

    final static Logger logger = Logger.getLogger(RUNNING_FILE.class);

    public static void main(String[] args) throws NoSuchAlgorithmException {
        PropertyConfigurator.configure("log4j.properties");
        String PlainText = "Prince Khanna MTECH Project Junit Test Cases!";
        String LETS_PUT_KEY = "KHANNAPRINCE4711";
        //the private key is a 64 bits long sequence
        //SecretKey key = KeyGenerator.getInstance("AES").generateKey();
        SecretKeySpec keySpec = new SecretKeySpec(LETS_PUT_KEY.getBytes(), "AES");
        LETS_ENCRYPT_AGNET_USER_PASSWORD_AT_DB_LEVEL aes = new LETS_ENCRYPT_AGNET_USER_PASSWORD_AT_DB_LEVEL();
        LETS_DECRYPT des = new LETS_DECRYPT();
        String cipherText = aes.LETS_GENERATE_ENCRYPT_VALUES(PlainText, keySpec);

        System.out.println("Encrypted text: " + cipherText);
        logger.info("Writing to Excel file.. " + cipherText);
        String SAVE_DECRYPT = des.LETS_HERE_DECRY_OUR_TEXT(cipherText, keySpec);
        System.out.printf("Decrypted text: %s%n", SAVE_DECRYPT);
        logger.info("Writing to Excel file.. " + SAVE_DECRYPT);
//		System.out.println("Decrypte text: "+aes.decrypt(cipherText, keySpec));
    }
}
